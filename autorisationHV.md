# Autoriser vagrant avec hyper V 
##### Quentin Guiheneuc
## I Autoriser vagrant avec hyper V 
 
 * Il faut le powershll en admin, La commende pour autoriser est :
    ```
    Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Hyper-V -All
    ```
## II Config  vagrant
 *  ```
    # -*- mode: ruby -*-
    # vi: set ft=ruby :

    Vagrant.configure("2") do |config|
    config.vm.provision "shell", path: "script.sh"
    
    config.vm.box = "centos/7"
    config.vm.box_check_update = false
    config.vbguest.auto_update = false
    config.ssh.keys_only = true
    config.ssh.insert_key = false

    config.vm.synced_folder ".", "/vagrant", disabled: true
    config.vm.hostname = "TP3"
    config.vm.network "private_network", 
        bridge: "eth0"
    
        config.vm.provider :hyperv do |hyperv|
            hyperv.maxmemory = 1024
            hyperv.memory = 1024
            hyperv.cpus = 3
            hyperv.vmname = "Linux Tp3"
        end
    end
    ```