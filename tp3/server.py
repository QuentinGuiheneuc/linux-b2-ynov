#!/usr/bin/python3

import sys
import http.server
Port = 1025
server_address = ("0.0.0.0", Port)

server = http.server.HTTPServer
handler = http.server.CGIHTTPRequestHandler
handler.cgi_directories = ["/server/"]
print("Serveur actif sur le port :", Port)

httpd = server(server_address, handler)
httpd.serve_forever()
