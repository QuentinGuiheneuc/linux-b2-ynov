# Systemd
##### Quentin Guiheneuc
## <span style="color: red"> ATENTION MA VM c'est Hyper V !!!</span>
<span style="color: red"> Ma configuration est totalement différent !! </span>
Voir la configuration [ICI](/autorisationHV.md) !!!

## 1. Services systemd
 * Afficher le nombre de services systemd dispos sur la machine :
    ```
    [vagrant@TP3 ~]$ systemctl -t service 
    auditd.service                                        loaded active running Security Auditing Service
    chronyd.service                                       loaded active running NTP client/server
    crond.service                                         loaded active running Command Scheduler
    dbus.service                                          loaded active running D-Bus System Message Bus
    getty@tty1.service                                    loaded active running Getty on tty1
    gssproxy.service                                      loaded active running GSSAPI Proxy Daemon
    hypervkvpd.service                                    loaded active running Hyper-V KVP daemon
    hypervvssd.service                                    loaded active running Hyper-V VSS daemon
    irqbalance.service                                    loaded active running irqbalance daemon
    kmod-static-nodes.service                             loaded active exited  Create list of required static device nodes for the current kernel
    network.service                                       loaded active exited  LSB: Bring up/down networking
    NetworkManager-wait-online.service                    loaded active exited  Network Manager Wait Online
    NetworkManager.service                                loaded active running Network Manager
   [...]
    systemd-udev-trigger.service                          loaded active exited  udev Coldplug all Devices
    systemd-udevd.service                                 loaded active running udev Kernel Device Manager
    systemd-update-done.service                           loaded active exited  Update is Completed
    systemd-update-utmp.service                           loaded active exited  Update UTMP about System Boot/Shutdown
    systemd-user-sessions.service                         loaded active exited  Permit User Sessions
    systemd-vconsole-setup.service                        loaded active exited  Setup Virtual Console
    tuned.service                                         loaded active running Dynamic System Tuning Daemon
    ```
* Afficher le nombre de services systemd actifs et en cours d'exécution ("running") sur la machine :

    ```
    [vagrant@TP3 ~]$ systemctl -t service | grep running
    auditd.service                                        loaded active running Security Auditing Service
    chronyd.service                                       loaded active running NTP client/server
    crond.service                                         loaded active running Command Scheduler
    dbus.service                                          loaded active running D-Bus System Message Bus
    getty@tty1.service                                    loaded active running Getty on tty1
    gssproxy.service                                      loaded active running GSSAPI Proxy Daemon
    hypervkvpd.service                                    loaded active running Hyper-V KVP daemon
    hypervvssd.service                                    loaded active running Hyper-V VSS daemon
    irqbalance.service                                    loaded active running irqbalance daemon
    NetworkManager.service                                loaded active running Network Manager
    polkit.service                                        loaded active running Authorization Manager
    postfix.service                                       loaded active running Postfix Mail Transport Agent
    rpcbind.service                                       loaded active running RPC bind service
    rsyslog.service                                       loaded active running System Logging Service
    serial-getty@ttyS0.service                            loaded active running Serial Getty on ttyS0
    sshd.service                                          loaded active running OpenSSH server daemon
    systemd-journald.service                              loaded active running Journal Service
    systemd-logind.service                                loaded active running Login Service
    systemd-udevd.service                                 loaded active running udev Kernel Device Manager
    tuned.service                                         loaded active running Dynamic System Tuning Daemon
    ```
* Afficher le nombre de services systemd qui ont échoué ("failed") ou qui sont inactifs ("exited") sur la machine : 
    ```
    [vagrant@TP3 ~]$ systemctl -t service | grep exited ;systemctl -t service | grep failed
    kmod-static-nodes.service                             loaded active exited  Create list of required static device nodes for the current kernel
    network.service                                       loaded active exited  LSB: Bring up/down networking
    NetworkManager-wait-online.service                    loaded active exited  Network Manager Wait Online
    rhel-dmesg.service                                    loaded active exited  Dump dmesg to /var/log/dmesg
    rhel-domainname.service                               loaded active exited  Read and set NIS domainname from /etc/sysconfig/network
    rhel-readonly.service                                 loaded active exited  Configure read-only root support
    selinux-policy-migrate-local-changes@targeted.service loaded active exited  Migrate local SELinux policy changes from the old store structure to the new structure
    sshd-keygen.service                                   loaded active exited  OpenSSH Server Key Generation
    systemd-hwdb-update.service                           loaded active exited  Rebuild Hardware Database
    systemd-journal-catalog-update.service                loaded active exited  Rebuild Journal Catalog
    systemd-journal-flush.service                         loaded active exited  Flush Journal to Persistent Storage
    systemd-machine-id-commit.service                     loaded active exited  Commit a transient machine-id on disk
    systemd-random-seed.service                           loaded active exited  Load/Save Random Seed
    systemd-remount-fs.service                            loaded active exited  Remount Root and Kernel File Systems
    systemd-sysctl.service                                loaded active exited  Apply Kernel Variables
    systemd-tmpfiles-setup-dev.service                    loaded active exited  Create Static Device Nodes in /dev
    systemd-tmpfiles-setup.service                        loaded active exited  Create Volatile Files and Directories
    systemd-udev-trigger.service                          loaded active exited  udev Coldplug all Devices
    systemd-update-done.service                           loaded active exited  Update is Completed
    systemd-update-utmp.service                           loaded active exited  Update UTMP about System Boot/Shutdown
    systemd-user-sessions.service                         loaded active exited  Permit User Sessions
    systemd-vconsole-setup.service                        loaded active exited  Setup Virtual Console
    ```
    * Afficher la liste des services systemd qui démarrent automatiquement au boot ("enabled") :

    ```
    [vagrant@TP3 ~]$ systemctl | grep enabled
    [vagrant@TP3 ~]$
    ```
## 2 Analyse d'un service

* Status du service Nginx :
    ```
    [vagrant@TP3 ~]$ systemctl status nginx
    ● nginx.service - The nginx HTTP and reverse proxy server
    Loaded: loaded (/usr/lib/systemd/system/nginx.service; disabled; vendor preset: disabled)
    Active: active (running) since Mon 2020-10-05 14:21:23 UTC; 57s ago
    Process: 678 ExecStart=/usr/sbin/nginx (code=exited, status=0/SUCCESS)
    Process: 676 ExecStartPre=/usr/sbin/nginx -t (code=exited, status=0/SUCCESS)
    Process: 673 ExecStartPre=/usr/bin/rm -f /run/nginx.pid (code=exited, status=0/SUCCESS)
    Main PID: 680 (nginx)
    CGroup: /system.slice/nginx.service
            ├─680 nginx: master process /usr/sbin/nginx
            ├─681 nginx: worker process
            ├─682 nginx: worker process
            └─683 nginx: worker process
    [vagrant@TP3 ~]$
    ```
* Afficher :
    ```[vagrant@TP3 ~]$ systemctl cat nginx
    # /usr/lib/systemd/system/nginx.service
    [Unit]
    Description=The nginx HTTP and reverse proxy server
    After=network.target remote-fs.target nss-lookup.target

    [Service]
    Type=forking
    PIDFile=/run/nginx.pid
    # Nginx will fail to start if /run/nginx.pid already exists but has the wrong
    # SELinux context. This might happen when running `nginx -t` from the cmdline.
    # https://bugzilla.redhat.com/show_bug.cgi?id=1268621
    ExecStartPre=/usr/bin/rm -f /run/nginx.pid
    ExecStartPre=/usr/sbin/nginx -t
    ExecStart=/usr/sbin/nginx
    ExecReload=/bin/kill -s HUP $MAINPID
    KillSignal=SIGQUIT
    TimeoutStopSec=5
    KillMode=process
    PrivateTmp=true

    [Install]
    WantedBy=multi-user.target
    [vagrant@TP3 ~]$
    ```

    * Description : 
        * ExecStart : Démarre le logiciel.
        * ExecStartPre : Commandes qui s'exécuteront avant le ExecStart.
        * PIDFile : 
        * Type :
        * ExecReload : Redémarre la config
        * Description : c'est une description
        * After : excute après ExecStart

## 3 Création d'un service
* [le fichier du service](tp3/serverweb.service)
## 4 Sauvegrade
 * je vais utiliser le scritp de sauvegarde que tu as fait avec ton accord (autoriser) !!
 
