# Dévloiement Automatise
##### Quentin Guiheneuc
## <span style="color: red"> ATENTION MA VM c'est Hyper V !!!</span>
<span style="color: red"> Ma configuration est totalement différent !! </span>
## I Autoriser vagrant avec hyper V 
 
 * Il faut le powershll en admin, La commende pour autoriser est :
    ```
    Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Hyper-V -All
    ```
## I. Déploiement simple
  Pour l'ajout du disque sur hyper-v avec le fichier vagrantfile, je bug.
  je vais le metre le disque manuelement aussi la hots.
  ### [Partie I](Deploiement_automatise/part_1) file vagrant

## II. Re-package 

```
PS C:\Users\guihe\Documents\HYPER V\Mvl> vagrant up
Bringing machine 'default' up with 'hyperv' provider...
==> default: Verifying Hyper-V is enabled...
==> default: Verifying Hyper-V is accessible...
    default: Configuring the VM...
    default: Setting VM Enhanced session transport type to disabled/default (VMBus)
==> default: Starting the machine...
==> default: Waiting for the machine to report its IP address...
    default: Timeout: 120 seconds
    default: IP: 192.168.42.176
==> default: Waiting for machine to boot. This may take a few minutes...
    default: SSH address: 192.168.42.176:22
    default: SSH username: vagrant
    default: SSH auth method: private key
==> default: Machine booted and ready!
==> default: Setting hostname...
==> default: Machine already provisioned. Run `vagrant provision` or use the `--provision`
==> default: flag to force provisioning. Provisioners marked to run always will still run.
```
```
PS C:\Users\guihe\Documents\HYPER V\Mvl> vagrant ssh
Last login: Wed Sep 30 13:51:41 2020 from 192.168.42.212
[vagrant@node1 ~]$ exit
logout
Connection to 192.168.42.176 closed.
```
```
==> box: Successfully added box 'centos7-custom' (v0) for 'hyperv'!
PS C:\Users\guihe\Documents\HYPER V\Mvl>  vagrant package --output b2-tp2-centos.box
==> default: Verifying Hyper-V is enabled...
==> default: Stopping the machine...
==> default: off
==> default: Exporting VM...
==> default: Compressing package to: C:/Users/guihe/Documents/HYPER V/Mvl/b2-tp2-centos.box
```
```
S C:\Users\guihe\Documents\HYPER V\Mvl> vagrant box add b2-tp2-centos b2-tp2-centos.box
==> box: Box file was not detected as metadata. Adding it directly...
==> box: Adding box 'b2-tp2-centos' (v0) for provider:
    box: Unpacking necessary files from: file://C:/Users/guihe/Documents/HYPER%20V/Mvl/b2-tp2-centos.box
    box:
==> box: Successfully added box 'b2-tp2-centos' (v0) for 'hyperv'!
PS C:\Users\guihe\Documents\HYPER V\Mvl>
```