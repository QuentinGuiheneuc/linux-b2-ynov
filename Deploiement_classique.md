# Dévloiement classique
##### Quentin Guiheneuc
* Partitionnement de disque avec LVM, on veut un disque de 5Go soit separe un 2Go et l'autre 3Go.
    * Deuxième disque de 5Go
        ``` 
        [quentin@localhost ~]$ lsblk
        NAME            MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
        fd0               2:0    1    4K  0 disk
        sda               8:0    0   10G  0 disk
        ├─sda1            8:1    0    1G  0 part /boot
        └─sda2            8:2    0    9G  0 part
            ├─centos-root 253:0    0    8G  0 lvm  /
            └─centos-swap 253:1    0    1G  0 lvm  [SWAP]
        sdb               8:16   0    5G  0 disk
        sr0              11:0    1 1024M  0 rom
        ```
    * On vas le partitionner :

        ```
        [quentin@localhost ~]$ sudo pvcreate /dev/sdb
        [sudo] password for quentin:
        Physical volume "/dev/sdb" successfully created.

        [quentin@localhost ~]$ sudo pvs
        PV         VG     Fmt  Attr PSize  PFree
        /dev/sda2  centos lvm2 a--  <9.00g    0
        /dev/sdb          lvm2 ---   5.00g 5.00g
        ```

        ```
        [quentin@localhost ~]$ sudo vgcreate site /dev/sdb
        Volume group "site" successfully created

        [quentin@localhost ~]$ sudo vgs
        VG     #PV #LV #SN Attr   VSize  VFree
        centos   1   2   0 wz--n- <9.00g     0
        site     1   0   0 wz--n- <5.00g <5.00g
        ```

        ```
        [quentin@localhost ~]$ sudo lvcreate -L 2G site -n site1
        Logical volume "site1" created.

        [quentin@localhost ~]$ sudo lvs
        LV    VG     Attr       LSize  Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
        root  centos -wi-ao---- <8.00g
        swap  centos -wi-ao----  1.00g
        site1 site   -wi-a-----  2.00g
        ```
        ```
        [quentin@localhost ~]$ sudo lvcreate -l 100%FREE site -n site2
        Logical volume "site2" created.

        [quentin@localhost ~]$ sudo lvs
        LV    VG     Attr       LSize  Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
        root  centos -wi-ao---- <8.00g
        swap  centos -wi-ao----  1.00g
        site1 site   -wi-a-----  2.00g
        site2 site   -wi-a----- <3.00g
        ```
    * Les points de montages.
        * temporere 
            ```
            [quentin@localhost ~]$ sudo mount /dev/site/site1 /mnt/site1
            ```
            à faire sur les de peratision !
        * automatique au boot :
        modifier le fichier dans /ect/fstab ajouter c'est deux lines
            ```
            /dev/site/site1 /mnt/site1 ext4 defaults 0 0
            /dev/site/site2 /mnt/site2 ext4 defaults 0 0
            ```
    
    * Configuration du pare feu
        ```
        [quentin@node1 ~]$ sudo firewall-cmd --add-port=22/tcp --permanent
        success
        [quentin@node1 ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
        success
        ```
    * Configuration Nginx que en port 80 :
        ```
        worker_processes 1;
        error_log nginx_error.log;
        events {
            worker_connections 1024;
        }

        http {
                server {
                        listen 80;
                        location /site1/ {
                                alias /mnt/site1/;
                                index index.html;
                        }
                        location /site2/ {
                                alias /mnt/site2/;
                                index index.html;
                        }
                }
            }
        ```
    * Configuration Nginx avec SSL :
        * creation du SSL: 
            ```
             openssl req -new -newkey rsa:2048 -days 365 -nodes -x509 -keyout server.key -out server.crt
            ```
            on ajoute dans le dossier /etc/pki/tls/certs/ 
            le fichier server.crt
            avec on fait un chmod 400 pour que tout le monde le li .
            ```
            [quentin@node1 certs]$ sudo chmod 400 server.crt
            [quentin@node1 certs]$ ls -al
            total 16
            drwxr-xr-x. 2 root root  135 Sep 28 10:20 .
            drwxr-xr-x. 5 root root   81 Sep 23 16:27 ..
            lrwxrwxrwx. 1 root root   49 Sep 23 16:27 ca-bundle.crt -> /etc/pki/ca-trust/extracted/pem/tls-ca-bundle.pem
            lrwxrwxrwx. 1 root root   55 Sep 23 16:27 ca-bundle.trust.crt -> /etc/pki/ca-trust/extracted/openssl/ca-bundle.trust.crt
            -rwxr-xr-x. 1 root root  610 Aug  8  2019 make-dummy-cert
            -rw-r--r--. 1 root root 2516 Aug  8  2019 Makefile
            -rwxr-xr-x. 1 root root  829 Aug  8  2019 renew-dummy-cert
            -r--------. 1 root root 1269 Sep 28 10:20 server.crt
            ```
            on ajoute dans le dissier /etc/pki/tls/private/ le fichier server.key avec on fait un chmod 400 pour que tout le monde le li .
            ```
            [quentin@node1 private]$ ls -al
            total 4
            drwxr-xr-x. 2 root root   24 Sep 28 10:22 .
            drwxr-xr-x. 5 root root   81 Sep 23 16:27 ..
            -rw-r--r--. 1 root root 1704 Sep 28 10:22 server.key
            [quentin@node1 private]$ sudo chmod 400 server.key
            [sudo] password for quentin:
            [quentin@node1 private]$ ls -al
            total 4
            drwxr-xr-x. 2 root root   24 Sep 28 10:22 .
            drwxr-xr-x. 5 root root   81 Sep 23 16:27 ..
            -r--------. 1 root root 1704 Sep 28 10:22 server.key
            [quentin@node1 private]$
            ```
        * config Nginx :
        ```
        worker_processes 1;
        error_log nginx_error.log;
        events {
                worker_connections 1024;
        }

        http {
                server {
                        #listen 80;
                        listen 192.168.1.11:443 ssl;
                        ssl on;
                        server_name         node1.tp1.b2;
                        keepalive_timeout   70;

                        ssl_certificate     /etc/pki/tls/certs/server.crt;
                        ssl_certificate_key /etc/pki/tls/private/server.key;
                        ssl_protocols       TLSv1 TLSv1.1 TLSv1.2;
                        ssl_ciphers         HIGH:!aNULL:!MD5;

                        location /site1/ {
                                alias /mnt/site1/;
                                index index.html;
                        }
                        location /site2/ {
                                alias /mnt/site2/;
                                index index.html;
                        }
                }
        }
        ```




