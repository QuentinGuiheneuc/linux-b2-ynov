#! /bin/sh
# cree par Guiheneuc Quentin
# intall pakages vim and for Hyper V
yum install linux-tools-3.11.0-15-generic -y
yum install hv-kvp-daemon-init -y
yum update -y
yum install vim -y
yum install epel-release -y
yum install nginx -y
yum install python3 -y
